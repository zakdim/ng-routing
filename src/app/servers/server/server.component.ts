import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';

import { ServersService } from '../servers.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};

  constructor(private serversService: ServersService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.data
      .subscribe(
        (data: Data) => {
          this.server = data['server'];
        }
      );
    // const id: number = +this.route.snapshot.params['id'];
    // console.log(`Init ServerComponent: id=${id}`);
    // this.server = this.serversService.getServer(id);
    // console.log(`Found server: ${JSON.stringify(this.server)}`);

    // this.route.params
    //   .subscribe((params: Params) => {
    //     console.log(`ServerComponent route params changed: id=${params['id']}`);
    //     this.server = this.serversService.getServer(+params['id']);
    //   });
  }

  onEdit() {
    // this.router.navigate(['servers', this.server.id, 'edit']);
    this.router.navigate(['edit'],
      {
        relativeTo: this.route,
        queryParamsHandling: 'preserve'
      }
    );

  }
}
